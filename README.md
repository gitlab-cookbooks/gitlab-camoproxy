# gitlab-camoproxy

Installs an instance of camoproxy (currently cactus/go-camo) for use in camouflaging requests to external images linked inline in markdown on GitLab.com.

## Using

Just include the gitlab-camoproxy::default recipe.
Configuration:
* Provide an 'hmac-key' in the gkms secrets container configured in `camoproxy.secrets`

