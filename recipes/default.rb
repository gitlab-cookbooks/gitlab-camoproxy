#
# Cookbook Name:: gitlab-camoproxy
# Recipe:: default
#
# Copyright (C) 2019 GitLab Inc.
#
# License: MIT
#

camoproxy_secrets = gitlab_camoproxy_secrets()

directory node['camoproxy']['dir'] do
  owner 'root'
  group 'root'
  mode '0755'
  recursive true
end

directory node['camoproxy']['log_dir'] do
  owner 'root'
  group 'root'
  mode '0755'
  recursive true
end

include_recipe 'ark::default'

dir_name = ::File.basename(node['camoproxy']['dir'])
dir_path = ::File.dirname(node['camoproxy']['dir'])

ark dir_name do
  url node['camoproxy']['binary_url']
  checksum node['camoproxy']['checksum']
  version node['camoproxy']['version']
  path dir_path
  owner 'root'
  group 'root'
  action :put
  notifies :restart, 'runit_service[camoproxy]'
end

group 'camoproxy' do
  gid '700'
  system true
end

user 'camoproxy' do
  uid '700'
  gid 'camoproxy'
  home '/home/camoproxy'
  shell '/bin/false'
  password '!'
  system true
end

include_recipe 'runit::default'
runit_service 'camoproxy' do
  default_logger true
  log_dir node['camoproxy']['log_dir']
  options(
    camoproxy_secrets: camoproxy_secrets
  )
end

include_recipe 'iptables-ng::install'

iptables_ng_rule '00-block-camoproxy-from-metadata-service' do
  name      '00-block-camoproxy-from-metadata-service'
  chain     'OUTPUT'
  table     'filter'
  ip_version 4
  rule       '-m owner --uid-owner 700 -d 169.254.169.254 -j REJECT'
  action     :create
end
