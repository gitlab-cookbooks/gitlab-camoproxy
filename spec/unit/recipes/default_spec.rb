#
# Cookbook:: gitlab-camoproxy
# Spec:: default
#
# Copyright:: 2019, The Authors, All Rights Reserved.

require 'spec_helper'

describe 'gitlab-camoproxy::default' do
  context 'default execution' do
    before do
      mock_secrets_path = 'test/integration/data_bags/secrets/secrets.json'
      secrets = JSON.parse(File.read(mock_secrets_path))

      # Actual arg values don't matter, just need to match the node attribs in the let(:chef_run) below
      expect_any_instance_of(Chef::Recipe).to receive(:get_secrets)
        .with('gkms', 'secrets', 'key').and_return(secrets)
    end

    let(:chef_run) do
      runner = ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '18.04') do |node|
        node.normal['camoproxy']['secrets'] = {
          'backend' => 'gkms',
          'path' => 'secrets',
          'key' => 'key',
        }
      end
      runner.converge(described_recipe)
    end

    it 'converges successfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'creates directories' do
      expect(chef_run).to create_directory('/opt/camoproxy').with(
        mode: '0755',
        owner: 'root',
        group: 'root',
        recursive: true
      )
      expect(chef_run).to create_directory('/var/log/camoproxy').with(
        mode: '0755',
        owner: 'root',
        group: 'root',
        recursive: true
      )
    end

    it 'downloads and extracts archive' do
      # This isn't an amazing test; integration tests with kitchen do the real useful work
      expect(chef_run).to put_ark('camoproxy')
    end

    it 'creates an runit service' do
      expect(chef_run).to enable_runit_service('camoproxy')
    end

    it 'creates specific user with correct uid' do
      expect(chef_run).to create_user('camoproxy').with(
        uid: '700',
        gid: 'camoproxy',
        home: '/home/camoproxy',
        shell: '/bin/false',
        password: '!',
        system: true
      )
    end
    it 'creates specific group with correct gid' do
      expect(chef_run).to create_group('camoproxy').with(
        gid: '700',
        system: true
      )
    end

    it 'creates an iptables rule rejecting access to GCP metadata service from camoproxy user' do
      expect(chef_run).to create_iptables_ng_rule('00-block-camoproxy-from-metadata-service')
        .with(
          name:      '00-block-camoproxy-from-metadata-service',
          chain:     'OUTPUT',
          table:     'filter',
          ip_version: 4,
          rule:       '-m owner --uid-owner 700 -d 169.254.169.254 -j REJECT',
          action:     [:create]
        )
    end
  end
end
