#
# Cookbook Name gitlab-camoproxy
# Attributes:: default
#
# Mainly for tests; actual implementations will probably use gkms
# path/key needs to be the file (.json) in tests/integration/data_bags
default['camoproxy']['secrets']['backend'] = 'chef_vault'
default['camoproxy']['secrets']['path'] = 'secrets'
default['camoproxy']['secrets']['key'] = 'secrets'

default['camoproxy']['dir'] = '/opt/camoproxy'
default['camoproxy']['log_dir'] = '/var/log/camoproxy'
default['camoproxy']['checksum'] = '37a896dcd54f1e81ac96c92d48b609ebdbbdb4f181d30fa02692adb077dd01af'
default['camoproxy']['version'] = '2.1.3'
default['camoproxy']['go_version'] = 'go1135'
default['camoproxy']['binary_url'] = "https://github.com/cactus/go-camo/releases/download/v#{node['camoproxy']['version']}/go-camo-#{node['camoproxy']['version']}.#{node['camoproxy']['go_version']}.linux-amd64.tar.gz"
default['camoproxy']['binary'] = '/opt/camoproxy/bin/go-camo'
default['camoproxy']['max_size'] = 10240
