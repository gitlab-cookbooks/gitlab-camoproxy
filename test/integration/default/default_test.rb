# # encoding: utf-8

# Inspec test for recipe gitlab-camoproxy::default

# The Inspec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

control 'basic-camoproxy-installation' do
  describe file('/opt/camoproxy') do
    it { should be_directory }
    its('owner') { should eq 'root' }
    its('group') { should eq 'root' }
    its('mode') { should cmp '0755' }
  end

  describe file('/opt/camoproxy/bin/go-camo') do
    it { should be_file }
    its('owner') { should eq 'root' }
    its('group') { should eq 'root' }
    its('mode') { should cmp '0755' }
  end

  describe file('/opt/camoproxy/bin/url-tool') do
    it { should be_file }
    its('owner') { should eq 'root' }
    its('group') { should eq 'root' }
    its('mode') { should cmp '0755' }
  end

  describe runit_service('camoproxy') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end

  describe user('camoproxy') do
    it { should exist }
    its('uid') { should eq 700 }
    its('gid') { should eq 700 }
    its('home') { should eq '/home/camoproxy' }
    its('shell') { should eq '/bin/false' }
  end

  describe group('camoproxy') do
    it { should exist }
    its('gid') { should eq 700 }
  end

  describe processes('/opt/camoproxy/bin/go-camo') do
    its('users') { should eq ['camoproxy'] }
  end

  describe file('/etc/iptables.d/filter/OUTPUT/00-block-camoproxy-from-metadata-service.rule_v4') do
    it { should exist }
    its('type') { should eq :file }
    its('size') { should be > 0 }
  end
end
