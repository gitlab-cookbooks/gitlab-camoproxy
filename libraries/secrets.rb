module Gitlab
  module CamoproxyCookbook
    def gitlab_camoproxy_secrets
      if (chef_vault = node['camoproxy']['chef_vault'])
        include_recipe 'chef-vault'

        chef_vault_item(chef_vault, node['camoproxy']['chef_vault_item'])
      else
        secrets_hash = node['camoproxy']['secrets']

        get_secrets(secrets_hash['backend'], secrets_hash['path'], secrets_hash['key'])
      end
    end
  end
end

Chef::Recipe.send(:include, Gitlab::CamoproxyCookbook)
