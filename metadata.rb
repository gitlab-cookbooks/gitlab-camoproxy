name 'gitlab-camoproxy'
maintainer 'GitLab Inc.'
maintainer_email 'ops-contact@gitlab.com'
license 'All Rights Reserved'
description 'Installs/Configures camoproxy for GitLab'
long_description 'Recipe used to install and manage camoproxy at GitLab Inc.'
version '1.0.7'

depends 'ark', '~> 3.1'
depends 'runit'
depends 'gitlab_secrets'
depends 'iptables-ng', '~> 4.0.0'
